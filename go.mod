module bitbucket.org/uwaplmlf2/mlf2session

go 1.13

require (
	bitbucket.org/uwaplmlf2/go-mlf2 v0.9.25
	github.com/pkg/errors v0.8.1
	github.com/streadway/amqp v0.0.0-20190827072141-edfb9018d271
)
