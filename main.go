// Mlf2session manages a communication session with an MLF2 float.
package main

import (
	"bufio"
	"bytes"
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"runtime"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/uwaplmlf2/go-mlf2/pkg/messages"
	"github.com/pkg/errors"
	"github.com/streadway/amqp"
)

const (
	DataExchange    = "data"
	CommandExchange = "commands"
	XmodemRecv      = "/usr/bin/rx -c -b"
	ZmodemRecv      = "/usr/bin/rz"
)

const USAGE = `
mlf2session [options] floatid

Manage a communications session for an MLF2 float.
`

var Version = "dev"
var BuildDate = "unknown"

var (
	showvers    = flag.Bool("version", false, "Show program version information and exit")
	server      = flag.String("server", "localhost:5672", "AMQP server address")
	credentials = flag.String("credentials", "", "AMQP username/password file")
	xmopts      = flag.String("xmopts", "-O", "XMODEM receive options")
	zmopts      = flag.String("zmopts", "", "ZMODEM receive options")
)

type FileMatch struct {
	Pattern  *regexp.Regexp
	Mimetype string
	Encoding string
	Newext   string
}

var FileMatcher []FileMatch
var QuickLookFile *regexp.Regexp

// Compile the file matching regular expressions
func init() {
	table := [][4]string{
		{`(?i).*\.ncz`, "application/x-netcdf", "gzip", ".nc"},
		{`(?i).*\.nc\.gz`, "application/x-netcdf", "gzip", ".nc"},
		{`(?i).*\.nc$`, "application/x-netcdf", "", ""},
		{`(?i)syslog\d*.txt$|errors\.txt$`, "text/x-syslog", "", ""},
		{`(?i)syslog\d*\.z|errors\.z|sbclog\.z`, "text/x-syslog", "gzip", ".txt"},
		{`(?i)syslog\d*\.txt\.gz|errors\.txt\.gz`, "text/x-syslog", "gzip", ""},
		{`(?i)bal\d*\.txt`, "text/csv", "", ""},
		{`(?i)sbclog_.*\.gz$`, "text/x-unixlog", "gzip", ".txt"},
		{`(?i)sbclog_.*\.txt$`, "text/x-unixlog", "", ""},
		{`(?i).*\.txt$`, "text/plain", "", ""},
		{`(?i).*\.txt.gz$`, "text/plain", "gzip", ""},
		{`(?i).*\.sx$`, "text/x-sexp", "", ""},
		{`(?i).*\.sxz`, "text/x-sexp", "gzip", ".sx"},
		{`(?i).*\.csv$`, "text/csv", "", ""},
		{`(?i).*\.xml`, "application/xml", "", ""},
		{`(?i)trk\d*\.z|eng\d*\.z|alt\d*\.z|bal\d*\.z`, "text/csv", "gzip", ".csv"},
		{`(?i)eng\d*\.csv\.gz`, "text/csv", "gzip", ".csv"},
		{`(?i).*\.jpg`, "image/jpeg", "", ""},
		{`(?i)00index$`, "text/x-listing", "", ""},
		{`(?i)00index\.z`, "text/x-listing", "gzip", ".txt"},
		{`(?i)00index\.gz`, "text/x-listing", "gzip", ".txt"},
		{`(?i).*\.tgz`, "application/x-tar", "", ""},
		{`(?i).*\.zip`, "application/zip", "", ""},
		{`(?i).*\.ntz`, "application/octet-stream", "gzip", ".ntk"},
		{`(?i)explog_.*\.jsz`, "application/x-ndjson", "gzip", ".ndjson"},
		{`(?i).*\.jsz`, "application/json", "gzip", ".json"},
		{`(?i).*\.jsn\.gz`, "application/json", "gzip", ".json"},
		{`(?i).*\.jsn$`, "application/json", "", ".json"},
		{`(?i).*\.ndjson$`, "application/x-ndjson", "", ".ndjson"},
		{`(?i).*\.ndjson\.gz`, "application/x-ndjson", "gzip", ".ndjson"},
	}

	QuickLookFile = regexp.MustCompile(`(?i)telem.*|ql.*|dataql.*`)
	FileMatcher = make([]FileMatch, 0, len(table))
	for _, e := range table {
		FileMatcher = append(FileMatcher,
			FileMatch{
				Pattern:  regexp.MustCompile(e[0]),
				Mimetype: e[1],
				Encoding: e[2],
				Newext:   e[3],
			})
	}
}

type rawStatus struct {
	Mimetype string
	Contents []byte
}

type ioStream struct {
	In, Out *os.File
}

func (s *ioStream) Read(p []byte) (int, error) {
	return s.In.Read(p)
}

func (s *ioStream) Write(p []byte) (int, error) {
	return s.Out.Write(p)
}

func getLine(ctx context.Context, rdr io.Reader) (string, error) {
	ch := make(chan string)

	go func() {
		defer close(ch)
		rdr := bufio.NewReader(rdr)
		s, err := rdr.ReadString('\n')
		if err == nil {
			ch <- strings.TrimRight(s, " \t\r\n")
		}
	}()

	select {
	case resp, ok := <-ch:
		if !ok {
			return "", errors.New("Read error")
		}
		return resp, nil
	case <-ctx.Done():
		break
	}

	return "", ctx.Err()
}

func ask(ctx context.Context, rw *ioStream, prompt string) (string, error) {
	rw.Out.Write([]byte(prompt))
	return getLine(ctx, rw.In)
}

func getStatus(ctx context.Context, rdr io.Reader) (*rawStatus, error) {
	var err error
	s := rawStatus{}
	s.Mimetype, err = getLine(ctx, rdr)
	if err != nil {
		return nil, errors.Wrap(err, "read mime-type")
	}

	b, err := getLine(ctx, rdr)
	if err != nil {
		return nil, errors.Wrap(err, "read status")
	}
	s.Contents = []byte(b)

	return &s, err
}

// Guess the type and encoding of an MLF2 file
func guessType(filename string) (string, string, string) {
	for _, fm := range FileMatcher {
		if fm.Pattern.MatchString(filename) {
			return fm.Mimetype, fm.Encoding, fm.Newext
		}
	}

	return "application/octet-stream", "", ""
}

// Publish the float status message to an AMQP broker
func publishStatus(ch *amqp.Channel, floatid int, s *rawStatus, headers amqp.Table) error {
	key := fmt.Sprintf("float-%d.status", floatid)
	msg := amqp.Publishing{
		Headers:         headers,
		ContentType:     s.Mimetype,
		DeliveryMode:    2,
		Timestamp:       time.Now(),
		ContentEncoding: "us-ascii",
		Body:            s.Contents,
	}

	log.Printf("Publishing %s status message to %q", s.Mimetype, key)
	return ch.Publish(
		DataExchange,
		key,
		false, // mandatory
		false, // immediate
		msg)
}

// Publish a float file to an AMQP broker
func publishFile(ch *amqp.Channel, floatid int, name string, data []byte,
	headers amqp.Table) error {

	mtype, enc, ext := guessType(name)
	if enc == "" && strings.HasPrefix(mtype, "text/") {
		data = bytes.TrimRight(data, "\x1a")
	}

	var filename string
	// Generate a unique filename for "quick look" data files
	if QuickLookFile.MatchString(name) {
		filename = fmt.Sprintf("ql-%s%s",
			time.Now().UTC().Format("20060102_150405"),
			filepath.Ext(name))
	} else {
		filename = name
	}

	if ext == "" {
		headers["filename"] = filename
	} else {
		if i := strings.LastIndex(filename, "."); i != -1 {
			headers["filename"] = filename[0:i] + ext
		}
	}
	defer delete(headers, "filename")

	key := fmt.Sprintf("float-%d.file", floatid)
	msg := amqp.Publishing{
		Headers:         headers,
		ContentType:     mtype,
		DeliveryMode:    2,
		Timestamp:       time.Now(),
		ContentEncoding: enc,
		Body:            data,
	}

	log.Printf("Publishing %q (%s) to %q", filename, mtype, key)
	return ch.Publish(
		DataExchange,
		key,
		false, // mandatory
		false, // immediate
		msg)
}

// Get all pending command cancellation messages for a float. A cancellation message
// contains a JSON encoded list of database IDs corresponding to command messages that
// have been cancelled by the user.
func checkCancellation(ch *amqp.Channel, floatid int) map[string]int {
	ids := make(map[string]int)
	qname := fmt.Sprintf("float-%d.cancellation", floatid)
	msg, ok, err := ch.Get(qname, true)
	if err != nil {
		log.Printf("checkCancellation: %v", err)
	}

	for ok {
		contents := make([]string, 0)
		err = json.Unmarshal(msg.Body, &contents)
		if err != nil {
			log.Printf("Cannot decode: %q", msg.Body)
		} else {
			for _, e := range contents {
				ids[e] = 1
			}
		}
		msg, ok, err = ch.Get(qname, true)
	}
	return ids
}

// Requeue any unprocessed cancellation messages
func requeueCancellation(ch *amqp.Channel, floatid int, ids map[string]int) error {
	contents := make([]string, 0)

	for k, v := range ids {
		if v == 1 {
			contents = append(contents, k)
		}
	}

	var (
		err error
		b   []byte
	)

	if b, err = json.Marshal(contents); err == nil {
		msg := amqp.Publishing{
			ContentType:  "application/json",
			DeliveryMode: 2,
			Timestamp:    time.Now(),
			Body:         b,
		}

		key := fmt.Sprintf("float-%d.cancellation", floatid)

		err = ch.Publish(
			CommandExchange,
			key,
			false, // mandatory
			false, // immediate
			msg)
	} else {
		log.Printf("Requeue failed: %v", err)
	}

	return err
}

// Forward commands from an AMQP broker to the float
func sendCommands(ctx0 context.Context, rw *ioStream, floatid int,
	ch *amqp.Channel, headers amqp.Table) error {
	var err error

	// Queues
	incoming := fmt.Sprintf("float-%d.commands", floatid)
	outgoing := fmt.Sprintf("float-%d.responses", floatid)

	ids := checkCancellation(ch, floatid)

	msg, ok, err := ch.Get(incoming, false)
	if err != nil {
		return errors.Wrap(err, "sendCommands")
	}

	for ok {
		// Check if this command has been cancelled
		if db_id, has_id := msg.Headers["db_id"].(string); has_id {
			if ids[db_id] > 0 {
				log.Printf("Cancelling message: %q", db_id)
				msg.Ack(false)
				ids[db_id] += 1
				continue
			}
			headers["db_id"] = db_id
		} else {
			log.Printf("Warning, message has no database ID")
			delete(headers, "db_id")
		}

		if _, is_last := msg.Headers["eot"]; is_last {
			headers["eot"] = "yes"
		} else {
			delete(headers, "eot")
		}

		mc := messages.CommandReply{}
		mc.Command = strings.Trim(string(msg.Body), " \r\n")

		var timeout time.Duration
		// Allow a longer timeout for file requests because the float will not
		// respond until it has compressed and queued the file.
		if strings.HasPrefix(mc.Command, "send-file") {
			timeout = 120 * time.Second
		} else {
			timeout = 60 * time.Second
		}

		ctx, cancel := context.WithTimeout(ctx0, timeout)
		log.Printf("< %q", mc.Command)
		mc.Response, err = ask(ctx, rw, mc.Command+"\n")
		cancel()
		if err != nil {
			log.Printf("Command %q send failed: %v", mc.Command, err)
			// Requeue the command
			msg.Nack(false, true)
			break
		} else {
			msg.Ack(false)

			// Send the command and reply back to the sender
			if b, err := json.Marshal(mc); err == nil {
				reply := amqp.Publishing{
					Headers:      headers,
					ContentType:  "application/json",
					DeliveryMode: 2,
					ReplyTo:      msg.ReplyTo,
					Timestamp:    time.Now(),
					Body:         b,
				}

				err = ch.Publish(
					CommandExchange,
					outgoing,
					false, // mandatory
					false, // immediate
					reply)
			}
		}
		msg, ok, err = ch.Get(incoming, false)
	}

	return err
}

func getFile(ctx context.Context, rw *ioStream, filename string) ([]byte, error) {
	dir, err := ioutil.TempDir("", "incoming")
	if err != nil {
		return nil, errors.Wrap(err, "create temp dir")
	}
	defer os.RemoveAll(dir)
	tmpfn := filepath.Join(dir, filename)
	cmdline := strings.Split(XmodemRecv, " ")
	opts := strings.Split(*xmopts, " ")
	cmdline = append(cmdline, opts...)
	cmdline = append(cmdline, filename)
	cmd := exec.CommandContext(ctx, cmdline[0], cmdline[1:]...)
	cmd.Dir = dir
	cmd.Stdin = rw.In
	cmd.Stdout = rw.Out
	cmd.Stderr = rw.Out
	err = cmd.Start()
	if err != nil {
		return nil, errors.Wrap(err, "command start")
	}
	log.Printf("Waiting for: %v", cmdline)
	err = cmd.Wait()
	log.Printf("Exit status: %v", err)

	return ioutil.ReadFile(tmpfn)
}

func zgetFile(ctx context.Context, rw *ioStream, filename string) ([]byte, error) {
	dir, err := ioutil.TempDir("", "incoming")
	if err != nil {
		return nil, errors.Wrap(err, "create temp dir")
	}
	defer os.RemoveAll(dir)
	tmpfn := filepath.Join(dir, filename)

	var cmd *exec.Cmd
	cmdline := strings.Split(ZmodemRecv, " ")
	if *zmopts != "" {
		opts := strings.Split(*zmopts, " ")
		cmdline = append(cmdline, opts...)
	}

	if len(cmdline) > 1 {
		cmd = exec.CommandContext(ctx, cmdline[0], cmdline[1:]...)
	} else {
		cmd = exec.CommandContext(ctx, cmdline[0])
	}

	cmd.Dir = dir
	cmd.Stdin = rw.In
	cmd.Stdout = rw.Out
	cmd.Stderr = rw.Out
	err = cmd.Start()
	if err != nil {
		return nil, errors.Wrap(err, "command start")
	}
	log.Printf("Waiting for: %v", cmdline)
	err = cmd.Wait()
	log.Printf("Exit status: %v", err)

	return ioutil.ReadFile(tmpfn)
}

func makeHeaders(floatid int) amqp.Table {
	headers := make(amqp.Table)
	headers["floatid"] = int32(floatid)
	headers["imei"] = os.Getenv("IRSN")
	return headers
}

func runSession(ctx0 context.Context, rw *ioStream, floatid int, ch *amqp.Channel) error {
	var done bool

	for !done {
		ctx, cancel := context.WithTimeout(ctx0, 90*time.Second)
		resp, err := ask(ctx, rw, "?")
		cancel()
		if err != nil {
			return err
		}
		log.Printf("> %q", resp)
		words := strings.Split(resp, " ")
		switch words[0] {
		case "status":
			ctx, cancel := context.WithTimeout(ctx0, 90*time.Second)
			s, err := getStatus(ctx, rw)
			cancel()
			if err != nil {
				return err
			}
			publishStatus(ch, floatid, s, makeHeaders(floatid))
		case "put":
			data, err := getFile(ctx0, rw, words[1])
			if err != nil {
				return errors.Wrapf(err, "%q: %v", resp, err)
			}
			publishFile(ch, floatid, strings.ToLower(words[1]), data, makeHeaders(floatid))
		case "zput":
			data, err := zgetFile(ctx0, rw, words[1])
			if err != nil {
				return errors.Wrapf(err, "%q: %v", resp, err)
			}
			publishFile(ch, floatid, strings.ToLower(words[1]), data, makeHeaders(floatid))
		case "msgs?":
			err = sendCommands(ctx0, rw, floatid, ch, makeHeaders(floatid))
			rw.Out.Write([]byte("\n"))
			if err != nil {
				return err
			}
		case "bye":
			done = true
		}
	}
	return nil
}

func mqSetup(uri string, queues map[string]string) (*amqp.Channel, error) {
	conn, err := amqp.Dial(uri)
	if err != nil {
		return nil, errors.Wrap(err, "connect")
	}

	channel, err := conn.Channel()
	if err != nil {
		return nil, errors.Wrap(err, "open channel")
	}

	for qname, exch := range queues {
		if err = channel.ExchangeDeclare(
			exch,    // name of the exchange
			"topic", // type
			true,    // durable
			false,   // delete when complete
			false,   // internal
			false,   // noWait
			nil,     // arguments
		); err != nil {
			return nil, errors.Wrap(err, "exchange declare")
		}

		state, err := channel.QueueDeclare(
			qname, // name of the queue
			true,  // durable
			false, // delete when usused
			false, // exclusive
			false, // noWait
			nil,   // arguments
		)
		if err != nil {
			return nil, errors.Wrap(err, "queue declare")
		}

		if err = channel.QueueBind(
			state.Name, // name of the queue
			state.Name, // bindingKey
			exch,       // sourceExchange
			false,      // noWait
			nil,        // arguments
		); err != nil {
			return nil, errors.Wrap(err, "queue bind")
		}
	}

	return channel, nil
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, USAGE)
		flag.PrintDefaults()
	}

	flag.Parse()
	if *showvers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	args := flag.Args()
	if len(args) < 1 {
		flag.Usage()
		os.Exit(1)
	}

	floatid, err := strconv.Atoi(args[0])
	if err != nil {
		log.Fatalf("Cannot parse float-ID: %v", err)
	}

	user, password := "guest", "guest"
	if *credentials != "" {
		b, err := ioutil.ReadFile(*credentials)
		if err != nil {
			log.Fatalf("read credentials: %v", err)
		}
		f := strings.Split(string(b), ":")
		user = f[0]
		password = strings.TrimRight(f[1], " \t\r\n")
	}

	queues := make(map[string]string)
	queues["float.status"] = DataExchange
	queues["float.file"] = DataExchange
	queues[fmt.Sprintf("float-%d.commands", floatid)] = CommandExchange
	queues[fmt.Sprintf("float-%d.cancellation", floatid)] = CommandExchange

	uri := fmt.Sprintf("amqp://%s:%s@%s/mlf2", user, password, *server)
	channel, err := mqSetup(uri, queues)
	if err != nil {
		log.Fatal(err)
	}

	rw := &ioStream{
		In:  os.Stdin,
		Out: os.Stdout,
	}

	log.Fatal(runSession(context.Background(), rw, floatid, channel))
}
