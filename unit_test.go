package main

import (
	"encoding/json"
	"testing"
)

func TestFileTypes(t *testing.T) {
	table := []struct {
		name, mtype, enc string
	}{
		{"syslog01.txt", "text/x-syslog", ""},
		{"bal0001.txt", "text/csv", ""},
		{"foo.txt", "text/plain", ""},
		{"syslog01.txt.gz", "text/x-syslog", "gzip"},
		{"SYSLOG01.z", "text/x-syslog", "gzip"},
		{"syslog0.z", "text/x-syslog", "gzip"},
		{"eng00001.z", "text/csv", "gzip"},
		{"eng00001.csv.gz", "text/csv", "gzip"},
		{"00INDEX.z", "text/x-listing", "gzip"},
		{"ql_000000_000000.ncz", "application/x-netcdf", "gzip"},
		{"ql_000000_000000.nc.gz", "application/x-netcdf", "gzip"},
		{"telem.ncz", "application/x-netcdf", "gzip"},
		{"sbclog_12345.gz", "text/x-unixlog", "gzip"},
		{"sbclog_12345.txt", "text/x-unixlog", ""},
		{"foo.jsz", "application/json", "gzip"},
		{"explog_1234_5678.jsz", "application/x-ndjson", "gzip"},
	}

	for _, e := range table {
		mtype, enc, _ := guessType(e.name)
		if mtype != e.mtype || enc != e.enc {
			t.Errorf("Wrong type for %q: %q, %q", e.name, mtype, enc)
		}
	}
}

func TestQLMatch(t *testing.T) {
	table := []struct {
		name  string
		match bool
	}{
		{"telem.ncz", true},
		{"telem01.ncz", true},
		{"telem.nc", true},
		{"env0001.ncz", false},
		{"dataql.sxz", true},
		{"data0001.sz", false},
	}

	for _, e := range table {
		if QuickLookFile.MatchString(e.name) != e.match {
			t.Errorf("Bad match on %q: %v", e.name, !e.match)
		}
	}
}

func TestCancellation(t *testing.T) {
	body := []byte(`["338608ad65d9ad499065df7780001a1b"]`)
	contents := make([]string, 0)
	err := json.Unmarshal(body, &contents)
	if err != nil {
		t.Fatal(err)
	}
	if len(contents) != 1 {
		t.Errorf("Bad array length; expected 1, got %d", len(contents))
	}
}
