FROM amd64/alpine:latest
MAINTAINER Mike Kenney <mikek@apl.uw.edu>

RUN apk add --no-cache jq curl
COPY ./apk/lrzsz-0.12.20-r0.apk /
RUN apk add --allow-untrusted /lrzsz-0.12.20-r0.apk && \
    rm -f /lrzsz-0.12.20-r0.apk

WORKDIR /
COPY credentials /.mqcreds
COPY entrypoint.sh /
COPY mlf2session /usr/bin/

ENV DB_SERVER http://mlf2data.apl.uw.edu:5984
ENV AMQP_SERVER mlf2data.apl.uw.edu
ENV MQCREDS /.mqcreds
ENV XMOPTS -O

VOLUME ["/var/log/mlf2"]
ENTRYPOINT ["/entrypoint.sh"]
