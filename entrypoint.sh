#!/bin/sh
#


# On a RUDICS login, the IRSN environment variable will be set to the Iridium
# Serial Number (aka IMEI) of the float.
if test -n "$IRSN"; then
    exec 2>> /var/log/mlf2/session.log
    date +'** %F %T %z' 1>&2
    echo "Call from $IRSN" 1>&2
    DB="mlf2db"
    VIEW="_design/status/_view/imei"

    # Look-up the float-ID associated with this Iridium Serial Number
    if test -z "$FLOATID"; then
	FLOATID=`curl -s -X GET \
	           "$DB_SERVER/$DB/$VIEW?key=%22${IRSN}%22&limit=1" |\
                    jq -c '.rows[0].value.floatid'`
    fi

    if test -n "$FLOATID"; then
        LOGFILE="/var/log/mlf2/session-f${FLOATID}.log"
        date +'** %F %T %z' >> $LOGFILE
	# Disable character echo to reduce latency, disable the conversion of
	# carriage-returns to line-feeds, and disable signal input
	stty -echo -icrnl -isig
	# Run the shell
	exec mlf2session \
             --credentials $MQCREDS \
	     --server $AMQP_SERVER \
             --xmopts "$XMOPTS" \
             $FLOATID 2>> /var/log/mlf2/session-f${FLOATID}.log
    else
	echo "Cannot determine float-ID for $IRSN" 1>&2
    fi
else
    exec "$@"
fi
